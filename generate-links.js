fetch('https://gitlab.com/api/v4/projects/beepgroup%2Fjavascript30/repository/tree')
    .then(response => response.json())
    .then(data => {
        console.log(data);
        const projectLinksContainer = document.getElementById('project-links');
        data.filter(item => item.type === 'tree').forEach(dir => {
            const link = document.createElement('a');
            link.href = `./${dir.name}/index-START.html`; // Assuming each directory has an index.html
            link.textContent = dir.name.replace(/-/g, ' '); // Optional: replace dashes with spaces for readability
            projectLinksContainer.appendChild(link);
            projectLinksContainer.appendChild(document.createElement('br'));
        });
    })
    .catch(error => console.error('Error fetching repository contents:', error));
